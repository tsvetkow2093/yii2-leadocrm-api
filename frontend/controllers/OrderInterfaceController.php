<?php

namespace frontend\controllers;

use frontend\models\OrderApi;
use frontend\models\OrderForm;
use yii\web\Controller;

/**
 * OrderController implements the CRUD actions for Orders model.
 */
class OrderInterfaceController extends Controller
{

    public function actionIndex($page = false)
    {
        $api = new OrderApi();
        $formModel = new OrderForm();
        if ($this->request->isPost) {
            $formModel->load($this->request->post());

            $result = $api->createOrder($formModel);
            $response = json_decode($result, true);

            if(!empty($response['message'])) {
                $message = $response['message'];
            } elseif (!empty($response[0]['message'])) {
                $message = $response[0]['message'];
            }
            else {
                $message = 'Order successfully created!';
            }

        }


        $orders = $api->getOrders($page);
        $ordersArray = json_decode($orders, true);

        return $this->render('index', [
            'model' => $formModel,
            'orders' => $ordersArray['items'],
            'pagination' => $ordersArray['_meta'] ?? null,
            'message' => $message ?? ''
        ]);
    }

}
