<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use linslin\yii2\curl\Curl;

class OrderApi extends Model
{
    const API_URL = 'http://api.vevee.tech/';
    const ORDERS_PATH = 'orders';

    public function getOrders($page = false)
    {
        $curl = new Curl();
        if($page) {
            $curl->setGetParams(['page' => $page]);
        }

        try {
            $orders = $curl->get(self::API_URL.self::ORDERS_PATH);
        } catch (\Exception $e) {
        }

        return $orders ?? null;
    }

    public function createOrder(OrderForm $formModel)
    {
        $curl = new Curl();
        try {
            $response = $curl->setHeaders([
                'accessToken' => '2j3gv5gu24v4'
            ])->setPostParams([
                'item_id' => $formModel->item_id,
                'created_at' => time(),
                'client_name' => $formModel->client_name,
                'client_email' => $formModel->client_email,
                'client_phone' => $formModel->client_phone,
                'client_ip' => $formModel->client_ip,
                'client_geo' => $formModel->client_geo,
                'code' => substr(md5(time()), 0, 20)
            ])->post(self::API_URL . self::ORDERS_PATH);
        } catch (\Exception $e) {
        }

        return $response ?? null;

    }

}
