<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "orders".
 *
 * @property int|null $item_id
 * @property string|null $client_name
 * @property string|null $client_email
 * @property string|null $client_phone
 * @property string|null $client_ip
 * @property string|null $client_geo
 * @property int|null $created_at
 * @property string|null $code
 * @property int|null $manager_id
 *
 */
class OrderForm extends Model
{
    public $item_id;
    public $manager_id;
    public $client_name;
    public $client_email;
    public $client_phone;
    public $client_ip;
    public $client_geo;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'created_at', 'manager_id'], 'integer'],
            [['client_name', 'client_phone', 'client_ip', 'client_geo'], 'string', 'max' => 255],
            [['item_id', 'manager_id', 'client_name', 'client_phone', 'client_ip', 'client_geo', 'client_email'], 'required'],
            [['client_email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'client_name' => 'Client Name',
            'client_email' => 'Client Email',
            'client_phone' => 'Client Phone',
            'client_ip' => 'Client Ip',
            'client_geo' => 'Client Geo',
            'created_at' => 'Created At',
            'manager_id' => 'Manager ID',
        ];
    }
}
