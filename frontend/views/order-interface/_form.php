<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\OrderForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ]]); ?>

    <div class="container">
        <div class="row">
            <div class="col-sm">
                <?= $form->field($model, 'item_id')->textInput() ?>
            </div>
            <div class="col-sm">
                <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm">
                <?= $form->field($model, 'client_email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm">
                <?= $form->field($model, 'client_phone')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                <?= $form->field($model, 'client_ip')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm">
                <?= $form->field($model, 'client_geo')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm">
                <?= $form->field($model, 'manager_id')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
