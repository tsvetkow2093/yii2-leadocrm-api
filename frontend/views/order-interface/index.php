<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

Pjax::begin(['enablePushState' => false]); ?>

<div class="orders-index">

    <?php echo $this->render('_form', ['model' => $model]); ?>

    <?php if(!empty($message)):?>
    <div class="alert alert-secondary" role="alert">
        <?=$message;?>
    </div>
    <?php endif;?>

    <?php echo $this->render('_list', ['orders' => $orders]); ?>

</div>
<hr/>
<nav aria-label="Page navigation example">
    <ul class="pagination">
        <?php for ($page = 1; $page <= $pagination['pageCount']; $page ++):?>
        <li class="page-item <?php echo ($page == $pagination['currentPage']) ? 'active' : '';?>"><a class="page-link" href="<?=Url::to(['/order-interface', 'page' => $page])?>"><?=$page?></a></li>
        <?php endfor;?>
    </ul>
</nav>

<?php Pjax::end(); ?>



