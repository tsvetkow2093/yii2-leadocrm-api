<?php if(!empty($orders)):?>
    <?php foreach ($orders as $order):?>
        <div class="card text-center mt-4">
            <div class="card-header">
                code: <?=$order['code']?>
            </div>
            <div class="card-body">
                <h5 class="card-title">client name: <?=$order['client_name']?></h5>
                <p class="card-text">client email: <?=$order['client_email']?></p>
                <p class="card-text">client phone: <?=$order['client_phone']?></p>
            </div>
            <div class="card-footer text-muted">
                <?=(new DateTime())->setTimestamp($order['created_at'])->format('H:i d.m.Y')?>
            </div>
        </div>
    <?php endforeach;?>
<?php endif;?>