<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders}}`.
 */
class m211029_195122_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(),
            'manager_id' => $this->integer(),
            'client_name' => $this->string(),
            'client_email' => $this->string(),
            'client_phone' => $this->string(),
            'client_ip' => $this->string(),
            'client_geo' => $this->string(),
            'created_at' => $this->integer(),
            'code' => $this->string(20),
        ], $tableOptions);

        $this->addForeignKey(
            '{{%fk-orders-item_id}}',
            '{{%orders}}',
            'item_id',
            '{{%items}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            '{{%fk-orders-manager_id}}',
            '{{%orders}}',
            'manager_id',
            '{{%managers}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%orders}}');
    }
}
