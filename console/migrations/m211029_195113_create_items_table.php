<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%items}}`.
 */
class m211029_195113_create_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%items}}', [
            'id' => $this->primaryKey(),
            'manager_id' => $this->integer(),
            'name' => $this->string(),
            'description' => $this->text(),
            'price' => $this->integer(),
            'manufacturer' => $this->string(),
        ], $tableOptions);

        $this->addForeignKey(
            '{{%fk-items-manager_id}}',
            '{{%items}}',
            'manager_id',
            '{{%managers}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%items}}');
    }
}
