<?php

namespace api\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "managers".
 *
 * @property int $id
 * @property string|null $access_code
 * @property string|null $name
 *
 * @property Items[] $items
 */
class UserIdentity extends ActiveRecord implements IdentityInterface

{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['access_code', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'access_code' => 'Access Code',
            'name' => 'Name',
        ];
    }

    public static function findIdentity($id)
    {
        throw new NotSupportedException('"findIdentity" is not implemented.');
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_code' => $token]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->access_code;
    }

    public function validateAuthKey($authKey)
    {
        return true;
    }

}
