<?php

namespace api\controllers;

use Yii;
use api\models\Items;
use api\models\Orders;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpHeaderAuth;
use yii\web\ForbiddenHttpException;


/**
 * Class OrderController
 * @package api\controllers
 */
class OrderController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = Orders::class;

    /**
     * @var string[]
     */
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpHeaderAuth::class,
            'header' => 'accessToken',
            'except' => ['index'],
        ];
        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);

        return $actions;
    }

    /**
     * @return ActiveDataProvider
     */
    public function actionIndex(): ActiveDataProvider
    {
        $query = Orders::find();
        $itemId = Yii::$app->request->get('item_id');
        $managerId = Yii::$app->request->get('manager_id');

        if($itemId) {
            $query->andWhere(['item_id' => $itemId]);
        }

        if($managerId) {
            $query->andWhere(['manager_id' => $managerId]);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 3,
            ],
        ]);
    }

    /**
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws ForbiddenHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        if ($action === 'create') {
            $itemId = Yii::$app->request->post('item_id');
            $managerId = Yii::$app->user->id;

            if (!$this->checkItemManager($itemId, $managerId)) {

                throw new ForbiddenHttpException(sprintf('You can only %s order with your items.', $action));
            }

            $clientName = Yii::$app->request->post('client_name') ?? '';
            $clientEmail = Yii::$app->request->post('client_email') ?? '';
            $clientPhone = Yii::$app->request->post('client_phone') ?? '';

            if ($this->checkClientOrder($clientName, $clientEmail, $clientPhone)) {

                throw new ForbiddenHttpException(sprintf('You can only %s order for new client.', $action));
            }
        }
    }

    /**
     * @param int $itemId
     * @param int $managerId
     * @return bool
     */
    private function checkItemManager(int $itemId, int $managerId): bool
    {
        return Items::find()->where([
            'id' => Yii::$app->request->post('item_id'),
            'manager_id' => Yii::$app->user->id])->exists();
    }

    /**
     * @param string $clientName
     * @param string $clientEmail
     * @param string $clientPhone
     * @return bool
     */
    private function checkClientOrder(string $clientName, string $clientEmail, string $clientPhone): bool
    {
        return Orders::find()
            ->orWhere(['client_name' => $clientName])
            ->orWhere(['client_email' => $clientEmail])
            ->orWhere(['client_phone' => $clientPhone])
            ->exists();
    }

}
